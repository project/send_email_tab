<?php

namespace Drupal\send_email_tab\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\node\NodeInterface;

class SendEmailTabForm extends FormBase {

  protected $node;

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "send_email_tab_form";
  }

  public function buildForm(array $form, FormStateInterface $form_state,  NodeInterface $node = NULL) {
   
    $this->node = $node;
    $config = $this->config('send_email_tab.settings');

    $form['send_email_tab']['emails'] = [
      '#type' => 'webform_email_multiple',
      '#title' => $this->t('E-mails para Newsletter'),
      '#required' => TRUE,
      '#size' => 100,
      '#description' => $this->t('Múltiplos emails devem ser separados por vírgula.'),
      '#default_value' => $config->get('emails'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

    $emails = trim($values['emails']);
    if(empty($emails)) {
      drupal_set_message(t('E-mails são obrigatórios'), 'error'); 
    } else {
      $form_state->setRedirect('send_email_tab.send_email', ['node' => $this->node->id(), 'emails' => $emails]);
    }

  }

}
