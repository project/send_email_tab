<?php

namespace Drupal\send_email_tab\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class SendEmailTabConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'send_email_tab_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'send_email_tab.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('send_email_tab.settings');
    $form['send_email_tab'] = [
      '#type' => 'details',
      '#title' => $this->t('Send Email Tab'),
      '#description' => $this->t(''),
      '#open' => TRUE,
    ];
    $form['send_email_tab']['header'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Header'),
      '#required' => TRUE,
      '#rows' => 10,
      '#default_value' => $config->get('header.value'),
    ];
    $form['send_email_tab']['footer'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Footer'),
      '#required' => TRUE,
      '#rows' => 10,
      '#default_value' => $config->get('footer.value'),
    ];
    $form['send_email_tab']['emails'] = [
      '#type' => 'webform_email_multiple',
      '#title' => $this->t('E-mail para Newsletter'),
      '#required' => TRUE,
      '#size' => 100,
      '#description' => $this->t('Múltiplos emails devem ser separados por vírgula.'),
      '#default_value' => $config->get('emails'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('send_email_tab.settings');
    $config->set('header', $values['header']);
    $config->set('footer', $values['footer']);
    $config->set('emails', $values['emails']);
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
