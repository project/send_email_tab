<?php

namespace Drupal\send_email_tab\Controller;

use Drupal\Core\Controller\ControllerBase;

use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\Component\Utility\Html;

use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\Core\Render\Markup;

class SendEmailTabController extends ControllerBase {

  public function send(NodeInterface $node, String $emails) {

    $header = Markup::create(Html::transformRootRelativeUrlsToAbsolute((string) \Drupal::config('send_email_tab.settings')->get('header.value'), \Drupal::request()->getSchemeAndHttpHost()));
    $middle = Markup::create(Html::transformRootRelativeUrlsToAbsolute((string) $node->body->value, \Drupal::request()->getSchemeAndHttpHost()));
    $footer = Markup::create(Html::transformRootRelativeUrlsToAbsolute((string) \Drupal::config('send_email_tab.settings')->get('footer.value'), \Drupal::request()->getSchemeAndHttpHost()));

    $body = $header . "<br>" . $middle  . "<br>" . $footer;

    $config = \Drupal::config('system.site');
    
    $mailManager = \Drupal::service('plugin.manager.mail');
    $module = 'send_email_tab';
    $key = 'send_email_tab_key';
    $params['from_name'] =  $config->get('name');
    $params['from_mail'] = $config->get('mail');
    $params['subject'] = $node->title->value;
    $params['message'] = $body;
    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $send = true;

    $tos = array_map('trim', explode(',', $emails));
    foreach($tos as $to) {
      $to = trim($to);
      $result = $mailManager->mail($module, $key, $to, $langcode, $params, $reply , $send);
    
      if ($result['result'] != true) {
        $message = t('There was a problem sending your email notification to @email.', array('@email' => $to));
        drupal_set_message($message, 'error');
        \Drupal::logger('send_email_tab')->error($message);
      } else {
        $message = t('An email has been sent to @email ', array('@email' => $to));
        drupal_set_message($message);
        \Drupal::logger('send_email_tab')->notice($message);
      }
  
    }  
      $url = Url::fromRoute('entity.node.canonical', ['node' => $node->nid->value])->toString();
      $response = new RedirectResponse($url);
      $response->send();
  }

}

